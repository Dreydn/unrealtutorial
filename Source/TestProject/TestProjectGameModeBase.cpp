// Fill out your copyright notice in the Description page of Project Settings.

#include "TestProject.h"
#include "TestProjectGameModeBase.h"

// we include our PlayerController
#include "TestPlayerController.h"

ATestProjectGameModeBase::ATestProjectGameModeBase()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATestPlayerController::StaticClass();
}
