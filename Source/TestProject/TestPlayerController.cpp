// Fill out your copyright notice in the Description page of Project Settings.

#include "TestProject.h"
#include "TestPlayerController.h"

ATestPlayerController::ATestPlayerController()
	: m_rotationSpeed(100.f)
	, m_rotationTag(TEXT("Rotate"))
{
	// we want the Tuck function to be called for our PlayerController
	PrimaryActorTick.bCanEverTick = true;

	// we reserve som memory in our array
	m_actors.Reserve(10);
}

void ATestPlayerController::BeginPlay()
{
	// we also call the BeginPlay for the parent class
	Super::BeginPlay();

	// we get every actor with our rotate tag
	UGameplayStatics::GetAllActorsWithTag(GetWorld(), m_rotationTag, m_actors);

	// generally display is the better log level but we use warning for now because it is highlighted in the console output
	UE_LOG(LogTemp, Warning, TEXT("We found %d Actors with the %s tag"), m_actors.Num(), *(m_rotationTag.ToString()))
}

void ATestPlayerController::Tick(float deltaTime)
{
	// we also call the tick for the parent class
	Super::Tick(deltaTime);

	// we prepare our delta rotator
	const FRotator rotation(0.0f, m_rotationSpeed * deltaTime, 0.0f);

	// now we iterate through the actors and add a rotation
	const int32 numActors = m_actors.Num();
	for (int32 i = 0; i < numActors; ++i)
	{
		// we add a rotaion in world space
		m_actors[i]->AddActorWorldRotation(rotation);
	}
}

