// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "TestPlayerController.generated.h"

/**
 * The default player Controller in the test project
 */
UCLASS()
class TESTPROJECT_API ATestPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	ATestPlayerController();

	// we overide the BeginPlay function which is called once the PlayerController has been spawned in the scene and is ready
	virtual void BeginPlay() override;

	// we override the Tick function which is called every frame
	virtual void Tick(float deltaTime) override;

private:

	// we use an array( the Unreal version of a dynamic array, like std::vector) to hold the pointers to the static meshes in the world
	UPROPERTY()
	TArray<class AActor*> m_actors;

	FName m_rotationTag;
	float m_rotationSpeed;
};
