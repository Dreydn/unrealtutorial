// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "TestProjectGameModeBase.generated.h"

/**
 * The GameMode for the test project
 */
UCLASS()
class TESTPROJECT_API ATestProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	ATestProjectGameModeBase();
};
